# 1. Læringsmål for WebDev_02

Hvad er CSS og hvordan applikeres CSS således, at den grafiske brugergrænseflade er i overensstemmelse med designeres grafiske retningslinjer.
- Hvad er CSS
- CSS specifikationer og placering af disse
- Basale CSS opsætning

# 2. Forløb 
Start med at se videoerne under punktet Forberedelse.
Lav øvelse - Se nederst. 

# 3. Forberedelse
Skitse anviser retningslinjer for designet af den mobile applikation i skal udvikle.

#### Welcome (1m 16s)
http://www.lynda.com/CSS-tutorials/Welcome/417645/484766-4.html?
#### Who is this course for (1m 59s)
http://www.lynda.com/CSS-tutorials/Who-course/417645/484767-4.html?
#### What is CSS (3m 26s)
http://www.lynda.com/CSS-tutorials/What-CSS/417645/484769-4.html?
#### Default browser styles (3m 39s)
http://www.lynda.com/CSS-tutorials/Default-browser-styles/417645/484770-4.html?
#### CSS Syntax (2m 46s)
http://www.lynda.com/CSS-tutorials/CSS-syntax/417645/484771-4.html?
#### Basic selector types (5m 24s)
http://www.lynda.com/CSS-tutorials/Basic-selector-types/417645/484772-4.html?
#### How CSS works with HTML structure (4m 51s)
http://www.lynda.com/CSS-tutorials/How-CSS-works-HTML-structure/417645/484773-4.html?
#### CSS authoring options (2m 41s)
http://www.lynda.com/CSS-tutorials/CSS-authoring-options/417645/484774-4.html?
#### How browsers apply styles (6m 6s)
http://www.lynda.com/CSS-tutorials/How-browsers-apply-styles/417645/484775-4.html?
#### Browser rendering difference (4m 37s)
http://www.lynda.com/CSS-tutorials/Browser-rendering-differences/417645/484776-4.html?
#### A Brief history of CSS (5m 52s)
http://www.lynda.com/CSS-tutorials/brief-history-CSS/417645/484778-4.html?
#### The current state of CSS (3m 37s)
http://www.lynda.com/CSS-tutorials/current-state-CSS/417645/484779-4.html?
#### Exploring CSS specification (7m 19s)
http://www.lynda.com/CSS-tutorials/Exploring-CSS-specifications/417645/484780-4.html?
#### Reading CSS specification (13m 12s)
http://www.lynda.com/CSS-tutorials/Reading-CSS-specifications/417645/484781-4.html?
#### Checking browser support (8m 47s)
http://www.lynda.com/CSS-tutorials/Checking-browser-support/417645/484782-4.html?
#### Working with fonts (4m 50s)
http://www.lynda.com/CSS-tutorials/Working-fonts/417645/484784-4.html?
#### Formatting text (8m)
http://www.lynda.com/CSS-tutorials/Formatting-text/417645/484785-4.html?
#### What is the box model? (5m 11s)
http://www.lynda.com/CSS-tutorials/What-box-model/417645/484786-4.html?
#### Margins & padding (4m 25s)
http://www.lynda.com/CSS-tutorials/Margins-padding/417645/484787-4.html?
#### Borders (4m 9s)
http://www.lynda.com/CSS-tutorials/Borders/417645/484788-4.html?
#### Backgrounds (5m 8s)
http://www.lynda.com/CSS-tutorials/Backgrounds/417645/484789-4.html?
#### Working with color (5m 52s)
http://www.lynda.com/CSS-tutorials/Working-color/417645/484790-4.html?
#### Common units of measurement (9m 11s)
http://www.lynda.com/CSS-tutorials/Common-units-measurement/417645/484791-4.html?
#### Element positioning (7m 4s)
http://www.lynda.com/CSS-tutorials/Element-positioning/417645/484792-4.html?
#### Floats (4m 49s)
http://www.lynda.com/CSS-tutorials/Floats/417645/484793-4.html?
#### Basic CSS layout concepts (5m 14s)
http://www.lynda.com/CSS-tutorials/Basic-CSS-layout-concepts/417645/484794-4.html?



# 4. Øvelse
#### 1
Opdater projektet  https://gitlab.com/knoerregaard/MobilDev.git

I det opdateres projekt forefindes WebDev_02 mappen. Med udgangspunkt i de partielt udført filer (eller jeres egen index.html), bedes du opsætte index.html og style.css således, at implementeringen er i overenstemmelse med
Website_LayoutTable.png (768px) og Website_LayoutLadingpage.png (960px).

#### 2
Push løsningen til Gitlab i et eget offentligt projekt, hvor dine kollegaer kan bidrage med input til evt. rettelser eller komme med synspunkter.
Del dit repo med en anden og bed dem om, at kommentere på indholdet.